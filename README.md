# ShiftPixy Code Challenge

<img src="https://admin.shiftpixy.com/assets/icons/pixy-logo.svg" width="450">

# Getting Started

- `git clone https://gitlab.com/shiftpixy-external/spx-cc.git`
- `yarn install`
- `yarn dev`

# Code Challenge

- [X] Using the existing codebase data, structure, etc., convert the json data in the client app into beautiful reusable UI cards.
- [X] Please make enough UI cards to overflow horizontally.
- [X] Make the row of UI cards display only one at a time with carousel behavior to access the other cards
- [X] Please follow all best practices you are aware of when it comes to security, secrets, UI, data, etc.

# Documentation:

- [Angular](https://angular.io)
- [Nest](https://nestjs.com)
- [Nx](https://nx.dev/)

Add a new color to theme. Set up default color variables.
