import { Injectable } from '@nestjs/common'
import { Card } from '@spx-cc/api-interfaces'

@Injectable()
export class AppService {
  getData(): Card[] {
    return [
      {
        id: 1,
        title: '1st Card',
        description: 'This is the 1st card to display',
        url: new URL('https://nottinghamsheriff.app'),
        ctaText: 'Learn More',
      },
      {
        id: 2,
        title: '2nd Card',
        description: 'This is the 2nd card to display',
        url: new URL('https://nottinghamsheriff.app'),
        ctaText: 'Learn More',
      },
      {
        id: 3,
        title: '3rd Card',
        description: 'This is the 3rd card to display',
        url: new URL('https://nottinghamsheriff.app'),
        ctaText: 'Learn More',
      },
      {
        id: 4,
        title: '4th Card',
        description: 'This is the 4th card to display',
        url: new URL('https://nottinghamsheriff.app'),
        ctaText: 'Learn More',
      },
      {
        id: 5,
        title: '5th Card',
        description: 'This is the 5th card to display',
        url: new URL('https://nottinghamsheriff.app'),
        ctaText: 'Learn More',
      },
    ]
  }
}
