import { Component } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Card } from '@spx-cc/api-interfaces'
import { Colors, Sizes } from '@spx-cc/ui'
import { DomSanitizer } from '@angular/platform-browser'
import { ConvertActionBindingResult } from '@angular/compiler/src/compiler_util/expression_converter'

@Component({
  selector: 'spx-cc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  cards$ = this.http.get<Card[]>('/api/cards')
  Colors = Colors
  Sizes = Sizes
  jsonHref: any

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {
    this.cards$
      .toPromise()
      .then(data => {
        const jsonString = JSON.stringify(data)
        const uri = this.sanitizer.bypassSecurityTrustUrl(
          'data:text/json;charset=UTF-8,' + encodeURIComponent(jsonString),
        )
        this.jsonHref = uri
      })
      .catch(error => {
        return Promise.reject(error)
      })
  }
}
