import { Directive, ElementRef, HostBinding, HostListener, Input, OnInit } from '@angular/core'
import { Colors, Sizes, Styles } from './button.enums'

@Directive({
  selector: 'button',
})
export class ButtonDirective implements OnInit {
  @Input() color: Colors = Colors.Primary
  @Input() size: Sizes = Sizes.Normal
  @Input() style: Styles = Styles.None
  @Input() fullWidth = false

  constructor(private el: ElementRef) {}

  protected IS_LOADING = 'is-loading'
  protected IS_FULL_WIDTH = 'is-fullwidth'
  ngOnInit() {
    this.el.nativeElement.classList.add('button')
  }

  @HostBinding('style.transition')
  get transition(): string {
    return 'all 300ms ease'
  }

  @HostBinding('class')
  get class(): DOMTokenList {
    this.el.nativeElement.classList.add(
      `is-${this.color}`,
      `is-${this.size}`,
      this.style ? `is-${this.style}` : '_',
      this.fullWidth ? this.IS_FULL_WIDTH : '_',
    )
    return this.el.nativeElement.classList.values()
  }

  @HostListener('click')
  onClick() {
    this.el.nativeElement.classList.add(this.IS_LOADING)
  }

  @HostListener('document:click', ['$event.target'])
  handleClick(target: unknown) {
    if (this.el.nativeElement.contains(target)) return
    this.el.nativeElement.classList.remove(this.IS_LOADING)
  }
}
