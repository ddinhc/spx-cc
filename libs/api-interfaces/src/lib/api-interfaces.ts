export interface Card {
  id: number
  title: string
  description: string
  url: URL
  ctaText?: string
  backgroundImageUrl?: URL
}
